#
# Required
#
variable "ipum_pod_name" {
  type = string
}

variable "network_id" {
  type = string
}

variable "bmc_subnet_id" {
  type = string
}

variable "gw_subnet_id" {
  type = string
}

variable "bmc_csv" {
  type = string
}

variable "gw_csv" {
  type = string
}

#
# Optional
#

variable "dns_base" {
  type    = string
  default = "openstack.example.net"
}
