# ipum-pod64-ports
Terraform Module: ipum-pod64-ports
==================================

This module builds upon the network generated from [ipum-1g-net](https://gitlab.com/openstack-vipu/terraform-modules/ipum-1g-net).  The purpose is to create Neutron ports for the BMC and gateway interfaces of each IPUM.   These won't be directly connected to anything in Openstack, but will be used to provide IP addresses to each interface via Neutron managed DHCP.

The module uses two provided CSV files (bmc_podXX.csv and gw_podXX.csv), that include the interface name, mac address and IP of the interface.   

Eg bmc_pod8.csv:

        hostname,mac,ip
        lr8_ipum1bmc,11:22:33:44:55:66,10.1.8.1
        lr8_ipum2bmc,22:33:44:55:66:77,10.1.8.2

or gw_pod8.csv

        hostname,mac,ip
        lr8_ipum1gw,33:44:55:66:77:88,10.2.8.1
        lr8_ipum2gw,44:55:66:77:88:99,10.2.8.2


Variables
=========
Required
--------
* ipum_pod_name - the name of the pod64/logical rack
* network_id - the openstack network the ports should be created on
* bmc_subnet_id - the subnet that the BMC ports should be attached to
* gw_subnet_id - the subnet that the GW ports should be attached to
* bmc_csv - the path to the CSV file describing the BMC ports
* gw_csv - the path the CSV describing the GW ports

Optional
--------
* dns_base - the DNS domain used for these ports (default: openstack.example.net)

Other Inputs
============
none

Usage
=====
The module is used in a single Terraform play, setup once when a new LR is integrated (or an IPUM replaced).   An example is seen [here](https://gitlab.com/openstack-vipu/graphcore-vipu-pod/-/blob/main/production/ipu-machines/main.tf#L32-40)

e.g.

        module "pod8" {
                source        = "git::ssh://git@gitlab.com/gc-openstack/terraform-modules/ipum-pod64-ports.git"
                ipum_pod_name = "pod8"
                network_id    = module.network.network_id
                bmc_subnet_id = module.network.bmc_subnet_id
                gw_subnet_id  = module.network.gw_subnet_id
                bmc_csv       = "bmc_pod8.csv"
                gw_csv        = "gw_pod8.csv"
        }

Development and Feedback
========================
The development of this tooling is ongoing, and any feedback is appreciated.  If you find any errors or bugs, please create an issue on the appropriate repository, or contact your Graphcore representative.