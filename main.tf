terraform {
  required_version = ">= 1.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.35"
    }
    local = {
      version = ">= 1.4"
    }
  }
}

locals {
  bmc = csvdecode(file(var.bmc_csv))
  gw  = csvdecode(file(var.gw_csv))
}

resource "openstack_networking_port_v2" "bmc_ports" {
  for_each = { for mapping in local.bmc :
  mapping.hostname => mapping if mapping.mac != "" }

  name        = replace(each.value.hostname, "_", "-")
  dns_name    = replace(each.value.hostname, "_", "-")
  network_id  = var.network_id
  mac_address = each.value.mac
  fixed_ip {
    subnet_id  = var.bmc_subnet_id
    ip_address = each.value.ip
  }
  admin_state_up = "true"
  tags           = ["bmc", var.ipum_pod_name]
}

resource "openstack_networking_port_v2" "gw_ports" {
  for_each = { for mapping in local.gw :
  mapping.hostname => mapping if mapping.mac != "" }

  name        = replace(each.value.hostname, "_", "-")
  dns_name    = replace(each.value.hostname, "_", "-")
  network_id  = var.network_id
  mac_address = each.value.mac
  fixed_ip {
    subnet_id  = var.gw_subnet_id
    ip_address = each.value.ip
  }
  admin_state_up = "true"
  tags           = ["gw", var.ipum_pod_name]
}
